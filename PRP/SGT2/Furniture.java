import java.awt.geom.*;
public class Furniture {
private Rectangle2D.Double r2d;
public Furniture(double x,double y,double w,double h)
{
r2d = new Rectangle2D.Double(x,y,w,h);
}
public boolean overlaps(Furniture f)
{
return r2d.intersects(f.getRect());
}
public Rectangle2D.Double getRect()
{
return r2d;
}
}
