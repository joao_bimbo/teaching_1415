import java.awt.geom.*;
public class Roomba {
private Arc2D.Double roomba;
public Roomba(double x,double y,double rad)
{
roomba = new Arc2D.Double(x-rad,y-rad,2*rad,2*rad,0,360,Arc2D.PIE);
}
public boolean inCollision(Furniture f)
{
return roomba.intersects(f.getRect());
}
}